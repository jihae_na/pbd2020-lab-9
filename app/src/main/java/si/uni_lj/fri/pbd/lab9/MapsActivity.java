package si.uni_lj.fri.pbd.lab9;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import si.uni_lj.fri.pbd.lab9.models.dto.WeatherResponseDTO;
import si.uni_lj.fri.pbd.lab9.models.dto.WeatherResponsesDTO;
import si.uni_lj.fri.pbd.lab9.rest.RestAPI;
import si.uni_lj.fri.pbd.lab9.rest.ServiceGenerator;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnCameraIdleListener {

    private GoogleMap mMap;
    private ConnectivityManager mNwManager;
    private ConnectivityManager.NetworkCallback
            mNwCallback;
    private boolean mOnUnmetered;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mOnUnmetered = false;
        mNwCallback = new ConnectivityManager.NetworkCallback(){
            @Override
            public void onCapabilitiesChanged(@NonNull Network network, @NonNull NetworkCapabilities networkCapabilities) {
                super.onCapabilitiesChanged(network, networkCapabilities);
                if(networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_NOT_METERED))
                {
                    mOnUnmetered = true;
                }
                else
                    mOnUnmetered = false;
            }
        };
        mNwManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        mNwManager.registerDefaultNetworkCallback(mNwCallback);


    }
    public void onDestroy() {

        super.onDestroy();
        mNwManager.unregisterNetworkCallback(mNwCallback);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnCameraIdleListener(this);

        mMap.moveCamera( CameraUpdateFactory.newLatLngZoom(new LatLng(46, 14.5),
                8.0f) );
    }


    @Override
    public void onCameraIdle()
    {
        if(mOnUnmetered == false)
        {
            System.out.println("not connect");
            Toast.makeText(getApplicationContext(),"Connect to an unmetered network, please",Toast.LENGTH_LONG).show();
        }
        else {
            RestAPI mRestClient;
            ServiceGenerator serviceGenerator = new ServiceGenerator();
            mRestClient = serviceGenerator.createService(RestAPI.class);
            String longitude_left = String.valueOf(mMap.getProjection().getVisibleRegion().latLngBounds.southwest.longitude);
            String latitude_bottom = String.valueOf(mMap.getProjection().getVisibleRegion().latLngBounds.southwest.latitude);
            String longitude_right = String.valueOf(mMap.getProjection().getVisibleRegion().latLngBounds.northeast.longitude);
            String latitude_top = String.valueOf(mMap.getProjection().getVisibleRegion().latLngBounds.northeast.latitude);
            String bbox = longitude_left + "," + latitude_bottom + "," + longitude_right + "," + latitude_top + ",10";

            Call<WeatherResponsesDTO> call = mRestClient.getCurrentWeatherData(bbox, Constants.API_KEY);
            call.enqueue(new Callback<WeatherResponsesDTO>() {
                @Override
                public void onResponse(Call<WeatherResponsesDTO> call, Response<WeatherResponsesDTO> response) {
                    if (response.isSuccessful()) {
                        WeatherResponsesDTO weather = response.body();
                        ArrayList<WeatherResponseDTO> weatherresponse = (ArrayList<WeatherResponseDTO>) weather.getWeatherResponses();
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(weatherresponse.get(0).getCoord().getLat(), weatherresponse.get(0).getCoord().getLon()))
                                .title(weatherresponse.get(0).getName() + " " + weatherresponse.get(0).getMain().gettemp())
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED
                                )));
                    }


                }

                @Override
                public void onFailure(Call<WeatherResponsesDTO> call, Throwable t) {

                }

            });
        }

    }

}
