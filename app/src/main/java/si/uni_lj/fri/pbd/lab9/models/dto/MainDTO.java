package si.uni_lj.fri.pbd.lab9.models.dto;

import com.google.gson.annotations.SerializedName;

//○ Main contains:
//■ “temp” - float
//■ “temp_min” - float
//■ “temp_max” - float
//■ “pressure” - float
//■ “sea_level” - float
//■ “grnd_level” - float
//■ “humidity” - float
public class MainDTO {
    @SerializedName("temp")
    private float temp;
    @SerializedName("temp_min")
    private float temp_min;
    @SerializedName("temp_max")
    private float temp_max;
    @SerializedName("pressure")
    private float pressure;
    @SerializedName("sea_level")
    private float sea_level;
    @SerializedName("grnd_level")
    private float grnd_level;
    @SerializedName("humidity")
    private float humidity;


    public float gettemp() {
        return temp;
    }
    public float gettemp_min() {
        return temp_min;
    }
    public float gettemp_max() {
        return temp_max;
    }
    public float getpressure() {
        return pressure;
    }
    public float getsea_level() {
        return sea_level;
    }
    public float getgrnd_level() {
        return grnd_level;
    }
    public float getHumidity() {
        return humidity;
    }

    // Setter Methods
    public void settemp( float temp ) {
        this.temp = temp;
    }
    public void settemp_min( float temp_min ) {
        this.temp_min = temp_min;
    }
    public void settemp_max(float temp_max)
    {
        this.temp_max = temp_max;
    }
    public void setpressure(float pressure)
    {
        this.pressure=pressure;
    }
    public void setsea_level(float sea_level)
    {
        this.sea_level = sea_level;
    }
    public void setGrnd_level(float grnd_level) {
        this.grnd_level = grnd_level;
    }
    public void setHumidity(float humidity)
    {
        this.humidity = humidity;
    }
}
