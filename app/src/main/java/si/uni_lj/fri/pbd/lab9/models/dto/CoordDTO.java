package si.uni_lj.fri.pbd.lab9.models.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
//■ “Lon” - float (longitude)
//■ “Lat” - float (latitude)

public class CoordDTO {
    @SerializedName("Lat")
    private float Lat;
    @SerializedName("Lon")
    private float Lon;
    public float getLat() {
        return Lat;
    }
    public float getLon() {
        return Lon;
    }
    // Setter Methods
    public void setLat( float Lat ) {
        this.Lat = Lat;
    }
    public void setLon( float Lon ) {
        this.Lon = Lon;
    }
}
