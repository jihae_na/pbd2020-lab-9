package si.uni_lj.fri.pbd.lab9.models.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WeatherResponseDTO {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("coord")
    private CoordDTO coord;
    @SerializedName("main")
    private MainDTO main;
    @SerializedName("dt")
    private int dt;
    // Getter Methods


    public CoordDTO getCoord() {
        return coord;
    }

    public int getDt() {
        return dt;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public MainDTO getMain() {
        return main;
    }

    public void setCoord(CoordDTO coord) {
        this.coord = coord;
    }

    public void setDt(int dt) {
        this.dt = dt;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setMain(MainDTO main) {
        this.main = main;
    }

    public void setName(String name) {
        this.name = name;
    }
}
